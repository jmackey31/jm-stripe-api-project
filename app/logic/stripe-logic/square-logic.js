const { Client, Environment } = require('square');
const client = new Client({
    environment: Environment.Sandbox,
    accessToken: process.env.SQUARE_ACCESS_TOKEN,
});

let squareLogicModule = {};

const createPayment = async () => {
    try {
        const response = await client.paymentsApi.createPayment({
            sourceId: null,
            idempotencyKey: null,
            amountMoney: {}
        });

        console.log('---------------------- start response');
        console.log(response);
        console.log('---------------------- end response');

        return {
            success: true,
            message: 'Testing create square payment.',
            data: response,
        };
    } catch (e) {
        return {
            success: false,
            message: 'There was an error while processing the square create payment.',
            error: e,
        }
    }
};

squareLogicModule.createPayment = async (req, res) => {
    const payment_response = await createPayment().then(response => response);

    if (!payment_response.success) {
        return res.json({
            success: payment_response.success,
            message: payment_response.message,
            error: payment_response.error,
        });
    } else {
        return res.json({
            success: payment_response.success,
            message: payment_response.message,
            paymentIntent: payment_response.paymentIntent,
        });
    }
};

module.exports = squareLogicModule;
