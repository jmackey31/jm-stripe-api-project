const stripe = require('stripe')(process.env.STRIPE_TEST_SECRET_KEY);

let stripeLogicModule = {};

const confirmCardPayment = async args => {
    console.log('---------- confirmCardPayment args.');
    console.log(args);
    console.log('---------- confirmCardPayment args END.');
    try {
        const {
            payment_intent_client_secret,
            cardElement,
            billers_name,
        } = args;

        // const check_confirm = await stripe.confirmCardPayment(payment_intent_client_secret, {
        //     payment_method: {
        //         card: cardElement,
        //         billing_details: {
        //             name: billers_name,
        //         },
        //     },
        // })
        // .then(result => {
        //     if (result.error) {
        //         console.log(result.error.message);
        //
        //         throw {
        //             success: false,
        //             message: result.error.message,
        //             error: result.error,
        //         }
        //     } else {
        //         // if (result.paymentIntent.status === 'succeeded') {
        //         //     // Show a success message to your customer
        //         //     // There's a risk of the customer closing the window before callback execution
        //         //     // Set up a webhook or plugin to listen for the payment_intent.succeeded event
        //         //     // to save the card to a Customer
        //         //
        //         //     // The PaymentMethod ID can be found on result.paymentIntent.payment_method
        //         // }
        //         console.log('---------- handleConfirmPayment result.');
        //         console.log(result);
        //         console.log('---------- handleConfirmPayment result END.');
        //
        //         return {
        //             success: true,
        //             message: 'Confirm Payment Response',
        //             confirmed_payment: result
        //         };
        //     }
        // })
        // .catch(e => {
        //     throw {
        //         success: false,
        //         message: 'Promise level catch error.',
        //         error: e,
        //     }
        // });

        const check_confirm = await stripe.confirmCardPayment(payment_intent_client_secret, {
            payment_method: {
                card: cardElement,
                billing_details: {
                    name: billers_name,
                },
            },
        }).then(response => response);
        console.log('---------- handleConfirmPayment check_confirm.');
        console.log(check_confirm);
        console.log('---------- handleConfirmPayment check_confirm END.');

        if (check_confirm.error) {
            console.log(check_confirm.error.message);

            throw {
                success: false,
                message: check_confirm.error.message,
                error: check_confirm.error,
            }
        } else {
            return {
                success: true,
                message: 'Confirm Payment Response',
                confirmed_payment: check_confirm
            };
        }
    } catch (e) {
        return {
            success: false,
            message: 'There was an error while handling this.',
            error: e,
        }
    }
};

const createPaymentIntent = async args => {
    try {
        const { stripe_cart_total, currency, customer_id } = args;

        const paymentIntent = await stripe.paymentIntents.create({
            amount: stripe_cart_total,
            currency,
            payment_method_types: ['card'],
            customer: customer_id,
        });

        return {
            success: true,
            message: 'Testing payment intents with stripe.',
            paymentIntent
        };
    } catch (e) {
        return {
            success: false,
            message: 'There was an error while handling this.',
            paymentIntent: {},
            error: e,
        }
    }
};

const customerCreate = async () => {
    try {
        const creationCheck = await stripe.customers.create().then(result => result);

        if (!creationCheck.id) {
            throw {
                success: false,
                message: 'There was an error while creating this customer.',
                customer: creationCheck,
            }
        } else {
            return {
                success: true,
                message: 'Customer info is ready.',
                customer: creationCheck,
            }
        }
    } catch (e) {
        return {
            success: false,
            message: 'There was an error while handling this.',
            error: e,
        }
    }
};

stripeLogicModule.customerCreate = async (req, res) => {
    const customerCreateResponse = await customerCreate().then(response => response);

    return res.json({
        success: customerCreateResponse.success,
        message: customerCreateResponse.message,
        customer: customerCreateResponse.customer
    });
};

stripeLogicModule.createPaymentIntent = async (req, res) => {
    const intent_response = await createPaymentIntent(req.body).then(response => response);

    if (!intent_response.success) {
        return res.json({
            success: intent_response.success,
            message: intent_response.message,
            error: intent_response.error,
        });
    } else {
        return res.json({
            success: intent_response.success,
            message: intent_response.message,
            paymentIntent: intent_response.paymentIntent,
        });
    }
};

stripeLogicModule.handleEntirePayment = async (req, res) => {
    const {
        stripe_cart_total, currency, customer_id, billers_name
    } = req.body;
    const cardElement = req.body.cardElement && req.body.cardElement.card ? req.body.cardElement.card : {};

    // 1. Create the Payment Intent.
    const payment_intent_check = await createPaymentIntent({
        stripe_cart_total, currency, customer_id
    }).then(payment_intent_response => payment_intent_response);
    // console.log('---------- payment_intent_check.');
    // console.log(payment_intent_check);
    // console.log('---------- payment_intent_check END.');

    // 2. Confirm The Card Payment.
    if (!payment_intent_check.success) {
        return res.json({
            success: payment_intent_check.success,
            message: payment_intent_check.message,
            data: payment_intent_check
        });
    } else {
        const { paymentIntent } = payment_intent_check;
        const { client_secret } = paymentIntent;
        const confirm_payment_check = await confirmCardPayment({
            payment_intent_client_secret: client_secret, cardElement, billers_name
        }).then(confirm_payment_response => confirm_payment_response);

        // 3. Return the response to the user either GOOD or BAD.
        return res.json({
            success: confirm_payment_check.success,
            message: confirm_payment_check.message,
            data: confirm_payment_check
        });
    }
};

module.exports = stripeLogicModule;
