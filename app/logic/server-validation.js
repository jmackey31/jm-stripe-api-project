let serverValidationModule = {};

// -----------------------------------------------------------------
// function to check values before inserting in query
serverValidationModule.validateAnyValue = (valueToCheck) => {
    return (valueToCheck !== null) &&
        (valueToCheck !== 'NaN') &&
        (valueToCheck !== '') &&
        (valueToCheck !== undefined) &&
        (valueToCheck !== false);
};

// -----------------------------------------------------------------
serverValidationModule.minimumCharacterLengthsCheck = (minlength, valueToCheck) => {
    return valueToCheck.length >= minlength;
};

// ____________________________________________________
// Check to see if number is an integer
serverValidationModule.isInteger = (x) => {
    return typeof x === 'number' && isFinite(x) && Math.floor(x) === x;
};

// ____________________________________________________
// Check to see if number is a floating point.
serverValidationModule.isFloat = (x) => {
    return !!(x % 1);
};

// ____________________________________________________
serverValidationModule.tofixed = (val) => {
    return val.toFixed(2);
};

// ____________________________________________________

// Phone number regex function
serverValidationModule.phoneNumberRegex = (fieldVal) => {
    const re = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

    return re.test(fieldVal);
};

// Validate that 10 digit phone number is true
serverValidationModule.checkValidPhoneNumber = (fieldVal) => {
    return (serverValidationModule.phoneNumberRegex(fieldVal) !== false) &&
        (fieldVal !== '') &&
        (fieldVal !== undefined) &&
        (fieldVal !== null);
};

// ____________________________________________________
// Validate that field is a number integer
serverValidationModule.checkNumberValue = (fieldVal) => {
    const newValNum = Number(fieldVal);

    return (Number.isInteger(newValNum)) &&
        (newValNum !== '') &&
        (newValNum !== undefined) &&
        (newValNum !== null);
};

// ____________________________________________________
serverValidationModule.deepNumberValueCheck = (fieldVal) => {
    let newValNum;

    if ((this.isFloat(fieldVal)) || (this.isInteger(fieldVal))) {
        newValNum = fieldVal;
    } else {
        newValNum = Number(fieldVal);
    }

    return (newValNum !== '') &&
        (newValNum !== 0) &&
        (newValNum !== undefined) &&
        (newValNum !== null);
};

// ____________________________________________________
serverValidationModule.checkCCNumber = (fieldVal) => {
    const newValNum = Number(fieldVal);

    return (Number.isInteger(newValNum)) &&
        (newValNum !== '') &&
        (newValNum !== undefined) &&
        (String(newValNum).length === 16) &&
        (newValNum !== null);
};

// ____________________________________________________
serverValidationModule.checkCVCNumber = (fieldVal) => {
    const newValNum = Number(fieldVal);

    return (Number.isInteger(newValNum)) &&
        (newValNum !== '') &&
        (newValNum !== undefined) &&
        (String(newValNum).length === 3) &&
        (newValNum !== null);
};

// ____________________________________________________
serverValidationModule.checkCCMonth = (fieldVal) => {
    const newValNum = Number(fieldVal);

    return (Number.isInteger(newValNum)) &&
        (newValNum !== '') &&
        (newValNum !== undefined) &&
        (String(newValNum).length === 2) &&
        (newValNum !== null);
};

// ____________________________________________________
serverValidationModule.checkCCYear = (fieldVal) => {
    const newValNum = Number(fieldVal);

    return (Number.isInteger(newValNum)) &&
        (newValNum !== '') &&
        (newValNum !== undefined) &&
        (String(newValNum).length === 4) &&
        (newValNum !== null);
};

// ____________________________________________________
// Check Zip Code String Length
serverValidationModule.checkZipCode = (fieldVal) => {
    return ((/^\s*\d{5}\s*$/.test(fieldVal)) !== false) &&
        (fieldVal !== '') &&
        (fieldVal !== undefined) &&
        (fieldVal !== null);
};

module.exports = serverValidationModule;
