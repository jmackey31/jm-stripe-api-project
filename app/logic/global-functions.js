let globalFunctionsModule = {};

globalFunctionsModule.testAPI = (req, res) => {
    return res.json({
        success: true,
        message: 'Make things happen!',
        words: [
            { word: 'Trees' },
            { word: 'Bees' },
            { word: 'Air' },
            { word: 'Water' }
        ]
    });
};

module.exports = globalFunctionsModule;
