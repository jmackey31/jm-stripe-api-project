const {
    testAPI,
} = require('../logic/global-functions');
const {
    customerCreate,
    createPaymentIntent,
    handleEntirePayment,
} = require('../logic/stripe-logic/stripe-logic');
const {
    createPayment,
} = require('../logic/stripe-logic/square-logic');

module.exports = (app, express) => {
    const API_ROUTER = express.Router(null);

    API_ROUTER.get('/test/yes', testAPI);

    // Stripe calls
    API_ROUTER.get('/customer/create', customerCreate);
    API_ROUTER.post('/payment-intent', createPaymentIntent);
    API_ROUTER.post('/handle-entire-payment', handleEntirePayment);

    // Square calls
    API_ROUTER.post('/square/create-payment', createPayment);

    API_ROUTER.get('/', (req, res) => {
        return res.json({ message: 'JM Stripe API Project.'});
    });

    return API_ROUTER;
};
