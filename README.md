# jm-stripe-api-project

* JM Stripe API Project

## -----------------------
## Objectives:
## -----------------------
### 1. Send a one time payment token object (from a frontend application to this api).
### a. Example object: (see PostMan Collections in this project).
### 2. Process that payment object via Stripe's Payment Intents API.
### 3. Return the response to the user.
