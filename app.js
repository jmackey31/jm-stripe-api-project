// app.js
const express             = require('express');
const app                 = express();
const bodyParser          = require('body-parser');
const cors                = require('cors');
const morgan              = require('morgan');
const config              = require('./config/config');
const methodOverride      = require('method-override');

const API_PREFIX_VAL = '/api_v1';

if (process.env.ENV_STATUS === 'local') {
    console.log('------ "' + process.env.ENV_STATUS + '" env ------');
    app.use(cors());
    app.use(morgan('dev'));
}

app.use(methodOverride());
app.use(express.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 50000}));
app.use(bodyParser.json());
app.use((req, res, next) => {
    res.set('X-Powered-By', process.env.API_POWERED_BY);
    next();
});

// Configure our app to handle CORS requests
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, \ Authorization');
    next();
});

// API ROUTES -------------------------
let apiRoutes = require('./app/routes/api')(app, express);
app.use(API_PREFIX_VAL, apiRoutes);

// START THE SERVER
app.listen(config.port);
console.log(`Listening on: http://localhost:${config.port}${API_PREFIX_VAL}`);
